# main entrypoints
bootstrap: check-bootstrap bootstrap-env install
update: update-deps install
distribute: build distribute-test check-distribute distribute-real

# individual components
bootstrap-env:
	pyenv versions | grep 3.10 || pyenv install 3.10
	pyenv virtualenv 3.10 ephemeral
	pyenv local ephemeral

build:
	python -m pip install twine wheel
	python -m build --wheel .

check-bootstrap:
	@echo "You should only need to run this once"
	@read -p "Are you sure? [y/N] " -n 1 ans && echo && [ $${ans:-N} = y ]

check-distribute:
	@echo "Check that the twine upload was successful to testpypi before continuing"
	@read -p "Do you want to distribute to PyPI? [y/N] " -n 1 ans && echo && [ $${ans:-N} = y ]

clean:
	rm -rf build/
	rm -rf dist/

distribute-real:
	twine upload --verbose --non-interactive -r pypi dist/*

distribute-test:
	twine upload --verbose --non-interactive -r testpypi dist/*

install:
	python -m pip install --upgrade pip wheel
	python -m pip install --upgrade -r requirements-dev.txt --editable .
	python -m pip check
	pyenv rehash

test:
	python -m pytest .

update-deps:
	python -m pip install --upgrade pip-tools pip wheel
	python -m piptools compile --resolver backtracking --output-file requirements.txt pyproject.toml
	python -m piptools compile --resolver backtracking --output-file requirements-dev.txt --extra dev pyproject.toml

# ensure these commands can always run
.PHONY: build bootstrap distribute install test update
