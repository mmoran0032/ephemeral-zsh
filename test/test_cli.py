from typer.testing import CliRunner

from ephemeral.cli import app

runner = CliRunner()


def test_default():
    result = runner.invoke(app, [])
    assert result.exit_code == 0


def test_version():
    result = runner.invoke(app, "--version")
    assert result.exit_code == 0
    assert result.output.startswith("[?] ephemeral")
