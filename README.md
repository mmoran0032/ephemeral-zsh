# ephemeral-sh

[![PyPI](https://img.shields.io/pypi/v/ephemeral-sh)](https://pypi.org/project/ephemeral-sh/)

A one-task tracker in your terminal with a forgetful history.

Ephemeral keeps track of a single task and when it was started. Once you decide to track something new, the old task can be forgotten or saved.

- See your current task: `ephemeral`
- Complete your current task: `ephemeral complete`
- Discard your current task: `ephemeral discard`
- Track a new task: `ephemeral track`
- View your history: `ephemeral history`
- Clear your history: `ephemeral clear`

> [!NOTE]
> Tired of writing `ephemeral`? You can also use the short alias `eph`.

All actions are automatically persisted to the filesystem to keep the CLI and the data in sync.
