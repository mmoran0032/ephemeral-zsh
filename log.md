# Development log

## 2025-01-06

- Hmm...it's not installing because of `pygments`?
- Wow, `pip-tools` generation is picking out the different versions. So it'
s not just a weird thing with Linux
- But then why does my installation fail in `make`? Let's run things sequentially.
- OK, we're trying to `pip install` with both. Since this is the code repo (you'd get the program through `pip` directly), let's stick with the `dev` requirements.

## 2024-11-24

- Making sure it runs fine on the macbook...some small issues, and some version inconsistencies

## 2024-11-23

- Doing some refactoring on the view. I'm basically slowly shifting toward a model-view-controller setup. My controller is still tightly coupled to the CLI, so maybe that's next?
- Yep, doing that clean-up too. It's a pretty big refactor, but it made sense to do both at the same time.
- I don't have much in the way of CLI tests, so everything still passes
- Did some refactoring, broke some tests, fixed the tests

## 2024-11-22

- Add a `distribute` manual step to CI and save credentials in GitLab
- Reinstalled `pyenv` using `curl https://pyenv.run | bash`
- Reccreating my environment with `make bootstrap-env`...fails. Trying to install Python 3.10 directly from `pyenv` also fails. So I must be missing some of the build requirements. Installing those now.
- OK, we're good! My `make check-*` commands aren't working, so maybe we just remove those?
- Another fun thing: I still have Anaconda Python here, so we're extra messy. I'm almost at the point where we do a spring cleaning on this machine again.
- OK, actual improvements to the app:
  - allow the new task to be defined on the command line
  - limit the history display so that each entry is only ever a single line
  - instead of rolling my own width limiter, use `rich.console.Console`
- HAHA! Publishing directly from GitLab works!
- Some tweaks to the code to standardize the display
- **Aside:** I still have "Yubikey required with `sudo`" enabled. Maybe I should figure out how to turn that off?
- **Aside:** I don't have my `neovim` configuration saved to `essentials`, so I'll need to make sure to do that at some time...and there's an issue in my `.zshrc` as well. Fun stuff, coming back to my desktop for development after so long. And it was just the Mac vs. Linux switch. I should figure out a better way to handle that.

## 2024-07-27

- Make `make bootstrap` more robust but checking for the right Python version, and if it's not there, install that python version before continuing
- Added a test to check the `--version` flag not working.
- Get `--version` flag working again by simplifying. I couldn't get `hidden=True` to work, or remove the extra `--no-version` possibility, but at least this seems to work fine.
- I need to mock out storage or history or something to do better tests...I don't think I can write any mor CLI tests without that taking place.
- Welp, I tried testing the CLI with the tracker mocked out, but I did it wrong so I borked my current tracker. That's fine, but let's spend some time testing our `model` objects since we don't have any tests there right now.
- Huh, my `make distribute` is also broken now...it doesn't recognize `twine`, and if I try to run `twine` directly, I get a credential error. That's not good. Did I make some change so that only CI can distribute that I don't remember? Nope, only linting and testing. But maybe I should consider that in the future?
  - It was a credential issue. I could either set some environment variables, or a `.pypirc` file. I'm doing the latter for now, but the environment variables might be better for the future, especially with CI.

## 2024-03-02

- Adding a `discard` command to dump the currenly-active task without completing it
  - Slash don't, since we have `clear --task` available
- Include direct `model.py` tests
- Add "short code" `eph` command
- Factor out response line options to make regular bracketed calls standardized
- You know what? Let's just get rid of the emojis for now...
- Also, new idea: `backup` and `restore` commands, or `backup create` and `backup restore`? Let's go with `backup` and subcommands.

Hmm...our version display is now broken...what did we have to do last time? It's unclear from these notes, so let's check `typer`'s documentation. Hmm...we're doing everything right according to [this](https://typer.tiangolo.com/tutorial/options/version/). So I don't know what's happening. Let's just ignore it for now.

In other news, we got too fancy with the `clear` command, so taking a step back to simplify it. We can `discard` the current task, or `clear` everything. Let's just stick with those.

Speaking of getting too fancy, the point of this is to be slightly forgetful and not caring that much about it, so let's not create backup functionality in the app. I did this manually while testing, but that can be done without exposing an API to the user. I think that's a better idea.

:face-palm: I forgot to update the readme...let's do that, then push up the new version.

OK, and let's also activate GitLab CI, by copying a base configuration from one of my other projects.

## 2023-07-23

- Adding in a `clear` command
- Using more built-in `typer` functionality
- Cleaning up the model state and CLI

## 2023-07-19

- I'm coming back to this...
- include `rich` for text formatting
- create `model.py` to control the task patterns
- update CLI to display task or prompt for a new task
  - looking for an emoji? `python -m rich.emoji | grep STRING`

## 2023-03-08

- `pip-tools` cannot be configured within `pyproject.toml`, so create a `Makefile`
  - `make update-deps` recompiles requirements files
  - `make init` installs everything you need and makes sure you're set
  - `make update` does both

## 2023-02-25

- install `typer` and add to the requirements
- create simple app and run with `python -m ephemeral.cli`
- add required and optional arguments
- include messages with style in response. Since `typer` uses `click` for styling, you need to specify `fg=*` as the foreground color, not just `color`.

## 2023-02-24

- create virtual environment with `pyenv virtualenv 3.10 ephemeral`
- set venv as project environment with `pyenv local ephemeral`
- create initial `pyproject.toml` file
- ensure Duo Security-specific configuration is set on an environment basis to avoid collisions with side projects. This involved setting up venv-specific `pip.conf` files so that the Duo Security-specific `index-url` is not being used for installing dependencies in non-Duo projects.
- install `pip-tools` in the venv using `pip`
- create requirements files with:

  ```sh
  pip-compile --resolver backtracking --output-file requirements.txt pyproject.toml
  pip-compile --resolver backtracking --output-file requirements-dev.txt --extra dev pyproject.toml
  ```

- install package and development dependencies with `python -m pip install -e ".[dev]"`
- install `ipython` in the venv using `pip`
- check that the package is available by importing and checking the version

At this point, it seems like all of our initial systems are set up and fine! We can start actually building the thing!
