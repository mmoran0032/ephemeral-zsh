import pytest

from ephemeral.model import Task, Tracker


@pytest.fixture
def mock_tracker() -> Tracker:
    return Tracker(path="/tmp/ephemeral")


def test_init(mock_tracker: Tracker) -> None:
    assert mock_tracker.current_task is None
    assert len(mock_tracker) == 0


def test_update(mock_tracker: Tracker) -> None:
    test_task = Task("test")
    assert len(mock_tracker) == 0
    mock_tracker.update("test")
    assert len(mock_tracker) == 1
    assert mock_tracker.current_task.id != test_task.id
    mock_tracker.update(test_task)
    assert len(mock_tracker) == 2
    assert mock_tracker.current_task.id == test_task.id


def test_clear(mock_tracker: Tracker) -> None:
    mock_tracker.update("test")
    assert len(mock_tracker) == 1
    mock_tracker.clear()
    assert len(mock_tracker) == 0

    # clearing again is a NOOP but otherwise allowed
    mock_tracker.clear()
    assert len(mock_tracker) == 0
